package com.a143040149.sista.tugas2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{
    private Button btntugas1, btntugas2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().setTitle("Tugas Sistem Aplikasi Bergerak");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btntugas1 = (Button)findViewById(R.id.btn_hitung_luas);
        btntugas2 = (Button)findViewById(R.id.btn_halamanutama);

        btntugas1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HitungLuas.class);
                startActivity(intent);
            }
        });

        btntugas2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SampleIntent.class);
                startActivity(intent);
            }
        });


    }
}